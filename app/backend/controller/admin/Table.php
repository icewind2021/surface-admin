<?php

namespace app\backend\controller\admin;

use app\backend\logic\Admin as AdminLogic;
use app\backend\model\Admin as AdminModel;
use surface\Component;
use surface\helper\FormAbstract;
use surface\helper\TableAbstract;
use surface\helper\Condition;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Selection;
use surface\table\components\Switcher;
use surface\table\components\Writable;

class Table extends TableAbstract
{

    private $rulePrefix = '/backend/admin';

    public function search(): ?FormAbstract
    {
        return new Search();
    }

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Component())->el('el-alert')->props(['title' => '我是一个自定义的el-alert标签，来看看怎么用吧', 'type' => 'error', 'effect'=>'dark']),
                (new Component())->el('div')->domProps('innerHTML', '<h2>管理员管理</h2><p style="color: #333333">演示Header中加入自定义HTML</p>'),
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix . '/delete'],
                    '确认删除选中列',
                    'id'
                ),
                (new Button('el-icon-plus', '增加'))->createPage($this->rulePrefix .  '/create'),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createSearch(),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', AdminModel::$labels['id'])),
            (new Column('id', AdminModel::$labels['id']))->props(['width' => '60px', 'sortable' => true]),
            (new Column('avatar', AdminModel::$labels['avatar']))->scopedSlots(
                [
                    (new Component())->el('el-image')->inject('attrs', ['src', 'array.preview-src-list'])->style(['width' => '50px', 'borderRadius'=>'50px']),
                ]
            )->props('width', '70px'),
            (new Column('username', AdminModel::$labels['username'])),
            (new Column('nickname', AdminModel::$labels['nickname']))->scopedSlots(
                [
                    (new Writable())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('status', AdminModel::$labels['status']))->scopedSlots(
                [
                    (new Switcher())->props(['async' => ['data' => ['id'], 'method' => 'POST', 'url' => $this->rulePrefix . '/change']]),
                ]
            )->props('width', '150px'),
            (new Column('create_time', AdminModel::$labels['create_time'])),
            (new Column('role', AdminModel::$labels['roles'])),
            (new Column('options', '操作'))->props('fixed', 'right')
                ->scopedSlots(
                    [
                        (new Button('el-icon-edit', '编辑'))
                            ->visible('_edit_visible')
                            ->createPage($this->rulePrefix . '/update', ['id'])->props('doneRefresh', true),
                        (new Button('el-icon-delete', '删除'))
                            ->visible('_delete_visible')
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix]),
                    ]
                ),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        return AdminLogic::tableList($where, $order, $page, $limit);
    }

}

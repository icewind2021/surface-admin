<?php

namespace app\backend\controller\attachment;

use app\backend\logic\Attachment as AttachmentLogic;
use app\backend\model\Attachment as AttachmentModel;
use surface\Component;
use surface\helper\FormAbstract;
use surface\helper\TableAbstract;
use surface\helper\Condition;
use surface\table\components\Button;
use surface\table\components\Column;
use surface\table\components\Selection;

class Table extends TableAbstract
{

    private $rulePrefix = '/backend/attachment';

    public function search(): ?FormAbstract
    {
        return new Search();
    }

    public function header(): ?Component
    {
        return (new Component(['el' => 'div']))->children(
            [
                (new Component())->el('div')->children(
                    [
                        (new Component())->el('el-breadcrumb')->children(
                            [
                                (new Component())->el('el-breadcrumb-item')->children(['系统设置']),
                                (new Component())->el('el-breadcrumb-item')->children(['附件管理']),
                                (new Component())->el('el-breadcrumb-item')->children(['附件列表']),
                            ]
                        ),
                        (new Component())->el('h2')->children(['附件管理']),
                        (new Component())->el('p')->children(['资源文件存储管理']),
                    ]
                ),
                (new Button('el-icon-close', '删除'))->createSubmit(
                    ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix.'/delete'],
                    '确认删除选中列',
                    'id'
                )->props('doneRefresh', true),
                (new Button('el-icon-refresh', '刷新'))->createRefresh(),
                (new Button('el-icon-search', '搜索'))->createSearch(),
            ]
        );
    }

    public function pagination(): ?Component
    {
        return (new Component())->props(
            [
                'async' => [
                    'url' => '',
                ],
            ]
        );
    }

    public function columns(): array
    {
        return [
            (new Selection('id', AttachmentModel::$labels['id'])),
            (new Column('id', AttachmentModel::$labels['id']))->props('width', '50px'),
            (new Column('uploader_name', AttachmentModel::$labels['uploader']))->props(['width' => '100px', 'show-overflow-tooltip' => true]),
            (new Column('url', AttachmentModel::$labels['img']))->scopedSlots(
                [
                    (new Component())->el('el-image')->inject('attrs', ['src', 'array.preview-src-list'])->style('width', '100px'),
                ]
            )->props('width', '100px'),
            (new Column('name', AttachmentModel::$labels['name'])),
            (new Column('ip', AttachmentModel::$labels['ip']))->props('width', '120px'),
            (new Column('size_label', AttachmentModel::$labels['size']))->props('width', '60px'),
            (new Column('wh', AttachmentModel::$labels['wh']))->props('width', '110px'),
            (new Column('mime', AttachmentModel::$labels['mime']))->props('width', '110px'),
            (new Column('suffix', AttachmentModel::$labels['suffix']))->props('width', '60px'),
            (new Column('create_time', AttachmentModel::$labels['create_time']))->props('width', '100px'),

            (new Column('options', '操作'))->props('fixed', 'right')
                ->scopedSlots(
                    [
                        (new Button('el-icon-delete', '删除'))
                            ->visible('_delete_visible')
                            ->props(['doneRefresh' => true])
                            ->createConfirm('确认删除数据？', ['method' => 'DELETE', 'data' => ['id'], 'url' => $this->rulePrefix . '/delete']),
                    ]
                )->props('width', '80px'),
        ];
    }

    public function data($where = [], $order = '', $page = 1, $limit = 15): array
    {
        return AttachmentLogic::tableList($where, $order, $page, $limit);
    }


}

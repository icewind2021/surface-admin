<?php
namespace app\backend\controller;

use app\BaseController;
use surface\helper\Curd;

/**
 *
 * 后台基类
 *
 * Class BackendController
 *
 * @package app\backend
 * Author: zsw zswemail@qq.com
 */
abstract class BackendController extends BaseController
{

    use Curd;

}

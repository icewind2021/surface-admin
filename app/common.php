<?php
// 应用公共文件

/**
 * 递归删除目录
 * @param $directory
 * Author: zsw zswemail@qq.com
 */
function del_dir($directory){//自定义函数递归的函数整个目录
    if(file_exists($directory)){//判断目录是否存在，如果不存在rmdir()函数会出错
        if($dir_handle=@opendir($directory)){//打开目录返回目录资源，并判断是否成功
            while($filename=readdir($dir_handle)){//遍历目录，读出目录中的文件或文件夹
                if($filename!='.' && $filename!='..'){//一定要排除两个特殊的目录
                    $subFile=$directory."/".$filename;//将目录下的文件与当前目录相连
                    if(is_dir($subFile)){//如果是目录条件则成了
                        del_dir($subFile);//递归调用自己删除子目录
                    }
                    if(is_file($subFile)){//如果是文件条件则成立
                        unlink($subFile);//直接删除这个文件
                    }
                }
            }
            closedir($dir_handle);//关闭目录资源
            rmdir($directory);//删除空目录
        }
    }
}

if ( ! function_exists('__')) {
    /**
     * 多语言
     *
     * @param string|array  $name [[$name1,$vars1,$lang1],[$name2,$vars2,$lang2]]
     * @param array         $vars
     * @param string        $lang
     * @return mixed|string
     * Author: zsw zswemail@qq.com
     */
    function __($name, $vars = [], $lang = '')
    {
        if (is_string($name)) {
            if (strpos($name, '?') === 0) {
                return \think\facade\Lang::has(ltrim($name, '?'));
            }
            return lang($name, $vars, $lang);
        } elseif (is_array($name)) {
            $str = "";
            foreach ($name as $v) {
                $str .= call_user_func_array('__', (array)$v);
            }

            return $str;
        }
    }
}

/**
 * index.php所在目录
 * @param string $path
 * @return string
 */
function index_path($path = '')
{
    return root_path('public') . ($path ? trim($path, '\\/') . DIRECTORY_SEPARATOR : '');
}

/**
 * 成功
 *
 * @param string $msg
 * @param array  $data
 * @param string $url
 * @param int    $wait
 * @param null   $debug
 *
 * @return \think\response\Json|\think\response\View
 */
function _success($msg = "", array $data = [], $url = '', int $wait = 3, $debug = null)
{
    return _show(0, $msg, $data, $url, $wait, $debug);
}

/**
 * 失败
 *
 * @param string $msg
 * @param array  $data
 * @param string $url
 * @param int    $wait
 * @param null   $debug
 *
 * @return \think\response\Json|\think\response\View
 */
function _error($msg = "", array $data = [], $url = '', int $wait = 3, $debug = null)
{
    return _show(1, $msg, $data, $url, $wait, $debug);
}


/**
 * 0成功 | >0 失败
 *
 * @param string $msg       可以为数组的一个语言包
 * @param int    $code
 * @param string $msg
 * @param array|string  $data
 * @param string $url
 * @param int    $wait
 * @param null   $debug
 *
 * @return \think\response\Json|\think\response\View
 */
function _show(int $code = 0, $msg = "", array $data = [], string $url = '', int $wait = 3, $debug = null)
{
    $url = (string)$url;

    if (strtolower(trim(request()->pathinfo(), '\\/')) == strtolower(trim($url, '\\/'))) {
        $wait = 0;
    }

    if (is_numeric($msg)) {
        $code = $msg;
        $msg = __('error_' . $code);
    } else if ($msg == '') {
        $msg = __($code);
    } else {
        $msg = __($msg);
    }

    $data = [
        'code' => $code,
        'wait' => $wait,
        'url' => $url,
        'msg'  => $msg,
        'data' => $data,
        'debug' => $debug
    ];

    if (!env('APP_DEBUG', false) || $debug === null) {
        unset($data['debug']);
    }

    if (request()->isAjax() || request()->isPost()) {
        unset($data['wait'], $data['url']);
        return json($data);
    }

    return view('/default/transfer', $data);
}

/**
 * 密码加密 校验
 *
 * @param        $password
 * @param string $check
 *
 * @return mixed
 * Author: zsw zswemail@qq.com
 */
function hashPassword(string $password, ?string $check = null)
{
    if ($check)
    {
        return password_verify($password, $check);
    } else
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}

/**
 * 格式化数据
 *
 * @param array  $options
 * @param string $labelName
 * @param string $valueName
 *
 * @return array
 */
function formatOptions(array $options, $labelName = 'label', $valueName = 'value'): array
{
    $data = [];
    foreach ($options as $k => $v) {
        array_push($data, [$labelName => $v, $valueName => $k]);
    }
    return $data;
}

if (!function_exists('getRandom')) {
    /**
     * 随机数生成
     * @param int $len
     * @param string $type
     * @return bool|string
     * Author: zsw zswemail@qq.com
     */
    function getRandom($len = 8, $type = 'alnum')
    {
        switch ($type) {
            case 'alpha':
            case 'alnum':
            case 'numeric':
            case 'nozero':
                switch ($type) {
                    case 'alpha':
                        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'alnum':
                        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'numeric':
                        $pool = '0123456789';
                        break;
                    case 'nozero':
                        $pool = '123456789';
                        break;
                }
                return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
            case 'unique':
            case 'md5':
                return md5(uniqid(mt_rand()));
            case 'encrypt':
            case 'sha1':
                return sha1(uniqid(mt_rand(), true));
        }
    }
}




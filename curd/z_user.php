<?php
 return array (
  'table' => 'z_user',
  'title' => '用户表',
  'description' => '用户表',
  'auto_timestamp' => true,
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
    0 => 
    array (
      'icon' => 'el-icon-open',
      'title' => '切换状态',
      'button_local' => 'right',
      'top_type' => 'page',
      'right_type' => 'confirm',
      'confirm_msg' => '确认修改用户状态',
      'url' => '/backend/user/change',
      'data_extend' => 
      array (
        'attr' => '我的附加参数',
        0 => 'id',
        1 => 'username',
      ),
      'btn_extend' => 
      array (
      ),
    ),
  ),
  'fields' => 
  array (
    'create_time' => 
    array (
      'title' => '注册时间',
      'field' => 'create_time',
      'default' => '0',
      'weight' => 3,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'date',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
        'type' => 'datetime',
      ),
      'save_format' => 
      array (
        0 => 'timestamp',
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'age' => 
    array (
      'title' => '年龄',
      'field' => 'age',
      'default' => '18',
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'sex' => 
    array (
      'title' => '性别',
      'field' => 'sex',
      'default' => '1',
      'weight' => 30,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'select',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_config',
      'option_config' => 
      array (
        1 => '男',
        2 => '女',
        3 => '公公',
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'status' => 
    array (
      'title' => '状态',
      'field' => 'status',
      'default' => '1',
      'weight' => 35,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'select',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_config',
      'option_config' => 
      array (
        1 => '正常',
        0 => '禁用',
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'avatar' => 
    array (
      'title' => '头像',
      'field' => 'avatar',
      'default' => '',
      'weight' => 40,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
        0 => ':<img src="{data}" width="50px">',
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'upload',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'password' => 
    array (
      'title' => '密码',
      'field' => 'password',
      'default' => '',
      'weight' => 40,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '不填写不会修改',
      'form_format' => 
      array (
        0 => ': ',
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
        0 => '\\app\\backend\\controller\\User::password',
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'username' => 
    array (
      'title' => '用户名',
      'field' => 'username',
      'default' => '',
      'weight' => '43',
      'search_type' => 'input',
      'search' => 'LIKE',
      'search_extend' => 
      array (
      ),
      'table_type' => 'writable',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => '',
      'weight' => 45,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
        'width' => '50px',
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
  ),
);